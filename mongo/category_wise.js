// group by each hash and category
var fb_category_hash_wise = function() {
	db.fb_category_hash_wise.drop();
	var bug_count = 0;
	var previous = "";
	db.fb_new.find().sort({
		project_hash : 1,
		category : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	db.fb_new.find().sort({
		project_hash : 1,
		category : 1
	}).forEach(
			function(doc) {
				if (doc['project_hash'] == previous['project_hash']
						&& doc['category'] == previous['category']) {
					bug_count = bug_count + 1;
				} else {
					previous['category_bug_count'] = bug_count;
					db.fb_category_hash_wise.insert(previous);
					bug_count = 1;
					previous = doc;
				}
			});
	db.fb_category_hash_wise.ensureIndex({
		project_hash : 1
	});
	db.fb_category_hash_wise.ensureIndex({
		project_name : 1
	});
	db.fb_category_hash_wise.ensureIndex({
		project_name : 1,
		category:1
	});
}

// for each project_name calculate the defect density per category

var dd_category_wise_project_name = function() {
	db.dd_category_wise_project_name.drop();
	var category_bug_count = 0;
	var previous = "";
	db.fb_category_hash_wise.find().sort({
		project_name : 1,
		category : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	db.fb_category_hash_wise
			.find()
			.sort({
				project_name : 1,
				category : 1
			})
			.forEach(
					function(doc) {
						if (doc['project_name'] == previous['project_name']
								&& doc['category'] == previous['category']) {
							category_bug_count = category_bug_count
									+ doc['category_bug_count'];
						} else {
							previous['category_bug_count'] = category_bug_count;
							db.dd_category_wise_project_name.insert(previous);
							category_bug_count = doc['category_bug_count'];
							previous = doc;
						}
					});
	db.dd_category_wise_project_name.ensureIndex({
		project_name : 1
	});
}

var dd_category_into_one_row = function() {
	db.dd_category_into_one_row.drop();
	var previous = "";
	db.dd_category_wise_project_name.find().sort({
		project_name : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	db.dd_category_wise_project_name.find().sort({
		project_name : 1
	}).forEach(function(doc) {
		if (doc["project_name"] == previous["project_name"]) {
			var category_name = doc['category'];
			previous[category_name + "_bug_count"] = doc['category_bug_count'];
		} else {
			db.dd_category_into_one_row.insert(previous);
			previous = doc;
			var category_name = doc['category'];
			previous[category_name + "_bug_count"] = doc['category_bug_count'];
		}
	});
	db.dd_category_into_one_row.ensureIndex({
		project_name : 1
	});
}
// append the project_stats
var append_project_stats = function() { 
	db.append_project_stats.drop();
	db.dd_usage.find().forEach(function(doc) {
		db.dd_category_into_one_row.find({
			project_name : doc['project_name']
		}).forEach(function(mdoc) {
			mdoc['sum_total_bug'] = doc['sum_total_bug'];
			mdoc['sum_total_size'] = doc['sum_total_size'];
			mdoc['project_defect_density'] = doc['defect_density'];
			db.append_project_stats.insert(mdoc);
		});
	});
	db.append_project_stats.ensureIndex({
		project_name : 1
	});
}
var usage_dd_category_wise_project_name = function() {
	db.usage_dd_category_wise_project_name.drop();
	print("started...");
	var outer = 0;
	var inner = 0;
	db.dd_usage.find().forEach(function(doc) {
		outer = outer + 1;
		db.append_project_stats.find({
			project_name : doc['project_name']
		}).forEach(function(mdoc) {
			inner = inner + 1;
			mdoc['usage'] = doc['usage'];
			populate_defect_density(mdoc, doc['sum_total_size']);
			db.usage_dd_category_wise_project_name.insert(mdoc);
		});
	})
	print("Done");
	print("outer: " + outer);
	print("inner: " + inner);
	print("checkout usage_dd_category_wise_project_name table, db.usage_dd_category_wise_project_name.find().pretty()");
}

var populate_defect_density = function(mdoc, size) {
	var columns = [ "EXPERIMENTAL", "MT_CORRECTNESS", "BAD_PRACTICE",
			"CORRECTNESS", "I18N", "PERFORMANCE", "SECURITY", "STYLE" ];

	columns.forEach(function(col) {
		var key = col + "_bug_count";
		var defect_density_key = col + "_defect_density";
		if (key in mdoc) {
			mdoc[defect_density_key] = (1000 * mdoc[key]) / size;
		}
	});
}
var run = function() {
	db.fb_new.ensureIndex({
		project_hash : 1,
		category : 1
	});
	fb_category_hash_wise();
	dd_category_wise_project_name();
	dd_category_into_one_row();
	append_project_stats();
	usage_dd_category_wise_project_name();
	print("mongoexport --csv -o usage_dd_category_wise_project_name.csv -d utilization -c usage_dd_category_wise_project_name -f maven_project_group,EXPERIMENTAL_bug_count,EXPERIMENTAL_defect_density,MT_CORRECTNESS_bug_count,MT_CORRECTNESS_defect_density,BAD_PRACTICE_bug_count,BAD_PRACTICE_defect_density,CORRECTNESS_bug_count,CORRECTNESS_defect_density,I18N_bug_count,I18N_defect_density,MALICIOUS_CODE_bug_count,MALICIOUS_CODE_defect_density,PERFORMANCE_bug_count,PERFORMANCE_defect_density,SECURITY_bug_count,SECURITY_defect_density,STYLE_bug_count,STYLE_defect_density,sum_total_bug,project_size,project_defect_density");
}
run();
// mongoexport --csv -o usage_dd_category_wise_project_name.csv -d utilization
// -c usage_dd_category_wise_project_name -f
// maven_project_group,EXPERIMENTAL_bug_count,EXPERIMENTAL_defect_density,MT_CORRECTNESS_bug_count,MT_CORRECTNESS_defect_density,BAD_PRACTICE_bug_count,BAD_PRACTICE_defect_density,CORRECTNESS_bug_count,CORRECTNESS_defect_density,I18N_bug_count,I18N_defect_density,MALICIOUS_CODE_bug_count,MALICIOUS_CODE_defect_density,PERFORMANCE_bug_count,PERFORMANCE_defect_density,SECURITY_bug_count,SECURITY_defect_density,STYLE_bug_count,STYLE_defect_density,sum_total_bug,project_size,project_defect_density


var removeCollections = function(){
	a= ['dd_category_wise_project_name',
	    'dd_fb_pmc',
	    'dd_hashwise',
	    'dd_maven_group',
	    'dd_maven_group_reduced',
	    'dd_usage',
	    'dd_usage_hashwise',
	    'dd_usage_new',
	    'dd_usage_reduced_categories',
	    'fb_category_append_maven_group',
	    'fb_category_hash_wise',
	    'fb_new',
	    'fb_norm',
	    'fb_pmc_join',
	    'fb_reduced',
	    'fb_reduced_selected_categories',
	    'fb_selected_categories',
	    'maven_usage',
	    'maven_usage_new',
	    'maven_usage_reduced_categories',
	    'pmc_new_distinct_pair',
	    'pmc_with_fs_project_name',
	    'pmc_with_fs_project_name_reduced_categories',
	    'proejct_defect_density',
	    'test',
	    'undefined',
	    'usage_dd_append_total_dd_maven_group',
	    'usage_dd_category_wise_project_name',
	    'usage_hashwise',
	    'usage_maven_groups'];
	for(i=0;i<a.length;i++){
		db[a[i]].drop();
	}
}

