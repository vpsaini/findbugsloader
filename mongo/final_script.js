// function to create fb_reduced
// this will have only one row for one hash. 
// we dont need multiple rows for one hash as every such row contains the version's total bug count and total size

var reduced_fb = function(output) {
    previous = "";
    db[output].drop();
    db.fb_new.find().sort({
        project_hash : 1
    }).forEach(function(doc) {
        if (doc['project_hash'] != previous) {
            db.fb_reduced.insert(doc);
            previous = doc['project_hash'];
        }
    });
    db[output].ensureIndex({
        project_hash : 1
    });
    db[output].ensureIndex({
        project_name : 1
    });
};

// for each hash in pmc_norm
// get project name for this hash from fb_reduced.
// get all rows for this project_name
// s = sum (project_size) and bc = sum (project_bug_count)
// calculate defect_density = (bc*1000)/s

/*
 * "_id" : ObjectId("51bea228f24a8a2db1fb4a63"), "usage" : 2101,
 * "maven_project_hash" : "a22958b18399506d188d0b4c211ef27d", "maven_project_id" :
 * 96718, "maven_project_name" : "org.apache.servicemix.bundles.gwt-user",
 * "maven_project_group" : "org.apache.servicemix.bundles", "project_name" :
 * "/home/sourcerer/processedbugprofile/org/apache/servicemix/servicemix-xmpp",
 * "sum_total_bug" : 251083, "sum_total_size" : 26667432, "defect_density" :
 */
var stats = function(pmc_unique_hash, fb_unique_hash, output) {
    db[output].drop();
    count = 0;
    fb_doc_count = 0;
    fb_count = 0;
    visited_projects = {};
    db[pmc_unique_hash].find().forEach(function(doc) {
        // print(doc['maven_project_hash']);
        try {
            var fb_new_rows_for_given_hash = db[fb_unique_hash].find({
                project_hash : doc['maven_project_hash']
            }).toArray();
            if (fb_new_rows_for_given_hash.length > 0) {
                var fb_doc = fb_new_rows_for_given_hash[0];
                if (fb_doc['project_name'] in visited_projects) {
                    throw BreakException;
                }
                visited_projects[fb_doc['project_name']] = 1;
                fb_doc_count = fb_doc_count + 1;
                // there should be only one row for each maven_project_hash
                // print(fb_doc['project_name']);
                sum_total_bug = 0;
                sum_total_size = 0;
                defect_density = 0;
                db[fb_unique_hash].find({
                    project_name : fb_doc['project_name']
                }).forEach(function(fb) {
                    fb_count = fb_count + 1;
                    // iterate on all the rows with this project_name
                    sum_total_bug = sum_total_bug + fb['project_bug_count'];
                    sum_total_size = sum_total_size + fb['project_size'];
                });
                if (0 != sum_total_size) {
                    defect_density = (1000 * sum_total_bug) / sum_total_size
                }
                doc['project_name'] = fb_doc['project_name'];
                doc['sum_total_bug'] = sum_total_bug;
                doc['sum_total_size'] = sum_total_size;
                doc['defect_density'] = defect_density;
                db[output].insert(doc);
            }
        } catch (e) {
            // do nothing
        }
        count = count + 1;
    });
    db[output].ensureIndex({
        project_name : 1
    });
    print('done');
    print("doc count is " + count);
    print("fb_count count is " + fb_doc_count);
    print("fb_count" + fb_count);
    db[output].ensureIndex({
        project_name : 1
    });
}

var pmc_with_fs_project_name = function(output) {
    db[output].drop();
    // pmc with file system project name
    print("started...");
    var count = 0;
    db["pmc_new"].find().forEach(function(doc) {
        var fb_new_rows_for_given_hash = db['fb_reduced'].find({
            project_hash : doc['maven_project_hash']
        }).toArray();
        if (fb_new_rows_for_given_hash.length > 0) {
            var fb_doc = fb_new_rows_for_given_hash[0];
            // there should be only one row for each maven_project_hash
            doc['project_name'] = fb_doc['project_name'];
            db[output].insert(doc);
            count = count + 1;
        }
    });
    db[output].ensureIndex({
        project_name : 1
    });
    print("done, project name appended to records: " + count);
}

var usage = function(pmc, output) {
    db[output].drop();
    print("started...");
    var usage = 0;
    previous = {};
    previous['project_name'] = "ignore";
    var count = 0;
    db[pmc].find().sort({
        project_name : 1
    }).forEach(function(doc) {
        if (doc['project_name'] == previous['project_name']) {
            usage = usage + 1;
        } else {
            count = count + 1;
            previous['usage'] = usage;
            db[output].insert(previous);
            usage = 1;
            previous = doc;
            delete previous['projects_that_imports'];
            delete previous['maven_project_hash'];
            delete previous['maven_project_id'];
        }
    });
    db[output].ensureIndex({
        project_name : 1
    });
    print("Done, maven usage calculated for projects " + count);
}

var update_stats = function(usage_collection, dd_collection, output) {
    db[output].drop();
    print("started...");
    var outer = 0;
    var inner = 0;
    db[usage_collection].find().forEach(function(doc) {
        outer = outer + 1;
        db[dd_collection].find({
            project_name : doc['project_name']
        }).forEach(function(mdoc) {
            inner = inner + 1;
            mdoc['usage'] = doc['usage'];
            mdoc['version_count'] = get_version_count(mdoc['project_name']);
            db[output].insert(mdoc);
        });
    })
    db[output].ensureIndex({
        project_name : 1
    });
    print("Done");
    print("outer: " + outer);
    print("inner: " + inner);
}
var update_stats_3 = function(usage_collection, dd_collection, output) {
    db[output].drop();
    print("started...");
    var outer = 0;
    var inner = 0;
    var rem = 0;
    db[usage_collection].find().forEach(function(doc) {
        count = 0;
        outer = outer + 1;
        db[dd_collection].find({
            project_name : doc['project_name']
        }).forEach(function(mdoc) {
            count = count + 1;
            inner = inner + 1;
            mdoc['usage'] = doc['usage'];
            mdoc['version_count'] = get_version_count(mdoc['project_name']);
            db[output].insert(mdoc);
        });
        if (count == 0) {
            rem = rem + 1;
            doc['version_count'] = get_version_count(doc['project_name']);
            db[output].insert(doc);
        }
    })
    db[output].ensureIndex({
        project_name : 1
    });
    print("Done");
    print("outer: " + outer);
    print("inner: " + inner);
    print("rem: " + rem);
}

var get_version_count = function(projectName) {
    return db.fb_reduced.find({
        project_name : projectName
    }).count();
}

var fb_selected_categories = function(output) {
    db[output].drop()
    db.fb_new.find(
            {
                category : {
                    $in : [ "MALICIOUS_CODE", "PERFORMANCE", "CORRECTNESS",
                            "EXPERIMENTAL", "MT_CORRECTNESS", "SECURITY" ]
                }
            }).forEach(function(doc) {
        db[output].insert(doc);
    });
    db[output].ensureIndex({
        project_hash : 1
    });
    db[output].ensureIndex({
        project_name : 1
    });
};

// create a new collection that will have unique project_hash
// project_bug_count will have the total bug count of this project
var fb_reduced_selected_categories = function() {
    db.fb_reduced_selected_categories.drop();
    previous = "";
    db.fb_selected_categories.find().sort({
        project_hash : 1
    }).limit(1).forEach(function(doc) {
        previous = doc;
    });
    var bug_count = 0;
    db.fb_selected_categories.find().sort({
        project_hash : 1
    }).forEach(function(doc) {
        if (doc['project_hash'] == previous['project_hash']) {
            // add the count
            bug_count = bug_count + 1;

        } else {
            previous['project_bug_count'] = bug_count;
            db.fb_reduced_selected_categories.insert(previous);
            previous = doc;
            bug_count = 1;
        }
    });
    db.fb_reduced_selected_categories.ensureIndex({
        project_hash : 1
    });
    db.fb_reduced_selected_categories.ensureIndex({
        project_name : 1
    });
};

// for style, bad practice and I18N
var fb_selected_categories_3 = function(output) {
    db[output].drop()
    db.fb_new.find({
        category : {
            $in : [ "I18N", "STYLE", "BAD_PRACTICE" ]
        }
    }).forEach(function(doc) {
        db[output].insert(doc);
    });
    db[output].ensureIndex({
        project_hash : 1
    });
    db[output].ensureIndex({
        project_name : 1
    });
};

// create a new collection that will have unique project_hash
// project_bug_count will have the total bug count of this project
var fb_reduced_selected_categories_3 = function(fb_selected_cat,
        fb_reduced_selected_cat) {
    db[fb_reduced_selected_cat].drop();
    previous = "";
    db[fb_selected_cat].find().sort({
        project_hash : 1
    }).limit(1).forEach(function(doc) {
        previous = doc;
    });
    var bug_count = 0;
    db[fb_selected_cat].find().sort({
        project_hash : 1
    }).forEach(function(doc) {
        if (doc['project_hash'] == previous['project_hash']) {
            // add the count
            bug_count = bug_count + 1;

        } else {
            previous['project_bug_count'] = bug_count;
            db[fb_reduced_selected_cat].insert(previous);
            previous = doc;
            bug_count = 1;
        }
    });
    db[fb_reduced_selected_cat].ensureIndex({
        project_hash : 1
    });
    db[fb_reduced_selected_cat].ensureIndex({
        project_name : 1
    });
};

// /////////////////////////////////
// group by each hash and category
var fb_category_hash_wise = function() {
    db.fb_category_hash_wise.drop();
    var bug_count = 0;
    var previous = "";
    db.fb_new.find().sort({
        project_hash : 1,
        category : 1
    }).limit(1).forEach(function(doc) {
        previous = doc;
    });
    db.fb_new.find().sort({
        project_hash : 1,
        category : 1
    }).forEach(
            function(doc) {
                if (doc['project_hash'] == previous['project_hash']
                        && doc['category'] == previous['category']) {
                    bug_count = bug_count + 1;
                } else {
                    previous['category_bug_count'] = bug_count;
                    db.fb_category_hash_wise.insert(previous);
                    bug_count = 1;
                    previous = doc;
                }
            });
    db.fb_category_hash_wise.ensureIndex({
        project_hash : 1
    });
    db.fb_category_hash_wise.ensureIndex({
        project_name : 1
    });
    db.fb_category_hash_wise.ensureIndex({
        project_name : 1,
        category : 1
    });
}

// for each project_name calculate the defect density per category

var dd_category_wise_project_name = function() {
    db.dd_category_wise_project_name.drop();
    var category_bug_count = 0;
    var previous = "";
    db.fb_category_hash_wise.find().sort({
        project_name : 1,
        category : 1
    }).limit(1).forEach(function(doc) {
        previous = doc;
    });
    db.fb_category_hash_wise.find().sort({
        project_name : 1,
        category : 1
    }).forEach(
            function(doc) {
                if (doc['project_name'] == previous['project_name']
                        && doc['category'] == previous['category']) {
                    category_bug_count = category_bug_count
                            + doc['category_bug_count'];
                } else {
                    previous['category_bug_count'] = category_bug_count;
                    db.dd_category_wise_project_name.insert(previous);
                    category_bug_count = doc['category_bug_count'];
                    previous = doc;
                }
            });
    db.dd_category_wise_project_name.ensureIndex({
        project_name : 1
    });
}

var dd_category_into_one_row = function() {
    db.dd_category_into_one_row.drop();
    var previous = "";
    db.dd_category_wise_project_name.find().sort({
        project_name : 1
    }).limit(1).forEach(function(doc) {
        previous = doc;
    });
    db.dd_category_wise_project_name.find().sort({
        project_name : 1
    }).forEach(function(doc) {
        if (doc["project_name"] == previous["project_name"]) {
            var category_name = doc['category'];
            previous[category_name + "_bug_count"] = doc['category_bug_count'];
        } else {
            db.dd_category_into_one_row.insert(previous);
            previous = doc;
            var category_name = doc['category'];
            previous[category_name + "_bug_count"] = doc['category_bug_count'];
        }
    });
    db.dd_category_into_one_row.ensureIndex({
        project_name : 1
    });
}
// append the project_stats
var append_project_stats = function() {
    db.append_project_stats.drop();
    db.dd_usage.find().forEach(function(doc) {
        db.dd_category_into_one_row.find({
            project_name : doc['project_name']
        }).forEach(function(mdoc) {
            mdoc['sum_total_bug'] = doc['sum_total_bug'];
            mdoc['sum_total_size'] = doc['sum_total_size'];
            mdoc['project_defect_density'] = doc['defect_density'];
            mdoc['maven_project_group'] = doc['maven_project_group'];
            mdoc['maven_project_name'] = doc['maven_project_name'];
            db.append_project_stats.insert(mdoc);
        });
    });
    db.append_project_stats.ensureIndex({
        project_name : 1
    });
}
var usage_dd_category_wise_project_name = function() {
    db.usage_dd_category_wise_project_name.drop();
    print("started...");
    var outer = 0;
    var inner = 0;
    db.dd_usage.find().forEach(function(doc) {
        outer = outer + 1;
        db.append_project_stats.find({
            project_name : doc['project_name']
        }).forEach(function(mdoc) {
            inner = inner + 1;
            mdoc['usage'] = doc['usage'];
            populate_defect_density(mdoc, doc['sum_total_size']);
            mdoc['version_count'] = get_version_count(mdoc['project_name']);
            db.usage_dd_category_wise_project_name.insert(mdoc);
        });
    })
    print("Done");
    print("outer: " + outer);
    print("inner: " + inner);
    print("checkout usage_dd_category_wise_project_name table, db.usage_dd_category_wise_project_name.find().pretty()");
}

var populate_defect_density = function(mdoc, size) {
    var columns = [ "EXPERIMENTAL", "MT_CORRECTNESS", "BAD_PRACTICE",
            "CORRECTNESS", "I18N", "PERFORMANCE", "SECURITY", "STYLE",
            "MALICIOUS_CODE" ];

    columns.forEach(function(col) {
        var key = col + "_bug_count";
        var defect_density_key = col + "_defect_density";
        if (key in mdoc) {
            mdoc[defect_density_key] = (1000 * mdoc[key]) / size;
        }
    });
}
var run = function() {
    db.fb_new.ensureIndex({
        project_hash : 1,
        category : 1
    });
    fb_category_hash_wise();
    dd_category_wise_project_name();
    dd_category_into_one_row();
    append_project_stats();
    usage_dd_category_wise_project_name();
    print("******************************");
    print("mongoexport --csv -o usage_dd_category_wise_project_name.csv -d "
            + "utilization -c usage_dd_category_wise_project_name -f "
            + "maven_project_name, maven_project_group,project_name,"
            + "EXPERIMENTAL_bug_count,EXPERIMENTAL_defect_density,MT_CORRECTNESS_bug_count,"
            + "MT_CORRECTNESS_defect_density,BAD_PRACTICE_bug_count,BAD_PRACTICE_defect_density,"
            + "CORRECTNESS_bug_count,CORRECTNESS_defect_density,I18N_bug_count,"
            + "I18N_defect_density,MALICIOUS_CODE_bug_count,MALICIOUS_CODE_defect_density,"
            + "PERFORMANCE_bug_count,PERFORMANCE_defect_density,SECURITY_bug_count,"
            + "SECURITY_defect_density,STYLE_bug_count,STYLE_defect_density,sum_total_bug,"
            + "project_size,project_defect_density,version_count");
    print("******************************");
    print("mongoexport --csv -o usage_dd_category_wise_project_name.csv -d utilization -c usage_dd_category_wise_project_name --fieldFile filefild.txt");
}
var addMissingProjectsToReducedCategories = function() {
    db.dd_fb_pmc_reduced_with_missing_projects.drop()
    db.dd_usage.find().forEach(
            function(doc) {
                var found = db.dd_usage_reduced_categories.find({
                    project_name : doc['project_name']
                }).toArray();
                if (null != found && found.length > 0) {
                    // correct the dd and sum_total_size
                    doc['sum_total_bug'] = found[0]['sum_total_bug']
                    doc['defect_density'] = (1000 * doc['sum_total_bug'])
                            / doc['sum_total_size']

                } else {
                    // this one is not present in dd_fb_pmc_reduced
                    doc['defect_density'] = 0
                    doc['sum_total_bug'] = 0
                }
                db.dd_fb_pmc_reduced_with_missing_projects.insert(doc)
            });
}
var ignoreMe = function() {
    db.ignoreMe.drop()
    var same = 0
    db.dd_usage.find().forEach(function(doc) {
        var found = db.dd_usage_reduced_categories.find({
            project_name : doc['project_name']
        }).toArray();
        if (null != found && found.length > 0) {
            // we already have dd and usage for this
            if (doc['sum_total_size'] == found[0]['sum_total_size']) {
                same = same + 1
            } else {
                db.ignoreMe.insert(doc)
            }
        } else {
        }
    });
}
var runme = function() {
    db.fb_new.ensureIndex({
        project_hash : 1
    });
    reduced_fb("fb_reduced");
    stats("pmc_norm", "fb_reduced", "dd_fb_pmc");// calculates the defect
    // density
    pmc_with_fs_project_name("pmc_with_fs_project_name");
    usage("pmc_with_fs_project_name", "maven_usage");
    update_stats("maven_usage", "dd_fb_pmc", "dd_usage");
    print("******************************");
    print("mongoexport --csv -o dd_usage_all.csv -d "
            + "utilization -c dd_usage -f _id,usage,maven_project_group,"
            + "maven_project_name,project_name,sum_total_bug,sum_total_size,defect_density,version_count");
    print("******************************");
    fb_selected_categories("fb_selected_categories");
    fb_reduced_selected_categories();
    stats("pmc_norm", "fb_reduced_selected_categories", "dd_fb_pmc_reduced");
    pmc_with_fs_project_name("pmc_with_fs_project_name_reduced_categories");
    usage("pmc_with_fs_project_name_reduced_categories",
            "maven_usage_reduced_categories");
    update_stats("maven_usage_reduced_categories", "dd_fb_pmc_reduced",
            "dd_usage_reduced_categories");
    print("******************************");
    print("mongoexport --csv -o dd_usage_reduced_categories.csv -d "
            + "utilization -c dd_usage_reduced_categories -f _id,usage,maven_project_group,"
            + "maven_project_name,project_name,sum_total_bug,sum_total_size,defect_density,version_count");
    print("******************************");
}
var run_reduced_3 = function() {
    fb_selected_categories_3("fb_selected_categories_3");
    fb_reduced_selected_categories_3("fb_selected_categories_3",
            "fb_reduced_selected_categories_3");
    stats("pmc_norm", "fb_reduced_selected_categories_3", "dd_fb_pmc_reduced_3");
    pmc_with_fs_project_name("pmc_with_fs_project_name_reduced_categories_3");
    usage("pmc_with_fs_project_name_reduced_categories_3",
            "maven_usage_reduced_categories_3");
    update_stats("maven_usage_reduced_categories_3", "dd_fb_pmc_reduced_3",
            "dd_usage_reduced_categories_3");
    print("******************************");
    print("mongoexport --csv -o dd_usage_reduced_categories_3_cats.csv -d "
            + "utilization -c dd_usage_reduced_categories_3 -f _id,usage,maven_project_group,"
            + "maven_project_name,project_name,sum_total_bug,sum_total_size,defect_density,version_count");
    print("******************************");
    // update_stats_3("maven_usage_reduced_categories_3", "dd_fb_pmc_reduced_3",
    // "dd_usage_reduced_categories_3_1");
    /*
     * print("mongoexport --csv -o dd_usage_reduced_categories_3_cats.csv -d " +
     * "utilization -c dd_usage_reduced_categories_3_1 -f
     * _id,usage,maven_project_group," +
     * "maven_project_name,project_name,sum_total_bug,sum_total_size,defect_density,version_count");
     * 
     */
}
map = function () {
    emit({project_name:this.project_name, projects_that_imports:this.projects_that_imports}, {count:1});
}

map = function () {
    emit({project_name:this.project_name}, {count:1});
}
reduce = function(k, values) {
    var result = {count: 0};
    values.forEach(function(value) {
        result.count += value.count;
    });
    return result;
}
//db.delme_pmc_fs_pn.mapReduce(map,reduce,{out: "usage_test"})
db.fb_reduced.mapReduce(map,reduce,{out: "version_count"})
db.delme_pmc_fs_pn.group( {
    key: { project_name: 1, projects_that_imports: 1 },
    reduce: function ( curr, result ) {
                result.total += 1;
            },
    initial: { total : 0 }
 } )
run_reduced_3();
// runme();
// run();

print("mongoexport --csv --fieldFile fields.txt --out usage_final_test.csv --db "
        + "utilization --collection usage_final_test2 -f project_name ,usage");
mongoexport --db utilization --collection usage_final_test2 --csv --fieldFile fields.txt --out usage_final_test.csv
mongoexport --db users --collection contacts --csv --fieldFile fields.txt --out /opt/backups/contacts.csv
db.usage_final_test.find().forEach(function(doc) {
   obj={};
   obj["project_name"]=doc._id.project_name;
   obj["usage"]=doc.value.count;
   db.usage_final_test2.insert(obj);
});



db.dd_fb_pmc.find().forEach(function(doc){
    doc['version_count']= get_version_count(doc['project_name']);
    db.newversioncount.insert(doc);
}