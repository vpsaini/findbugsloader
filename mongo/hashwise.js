var dd_hashwise = function() {
	db.dd_hashwise.drop();
	defect_density = 0;
	db.fb_reduced.find().forEach(function(doc) {
		defect_density = (1000 * doc['project_bug_count'] )/ doc['project_size'];
		doc['defect_density'] = defect_density;
		delete doc['maven_project_group'];
		delete doc['maven_project_id'];
		delete doc['project_name'];
		db.dd_hashwise.insert(doc);
	});
	db.dd_hashwise.ensureIndex({
		maven_project_hash : 1
	});
	db.pmc_new.ensureIndex({
		maven_project_hash : 1
	});
}

var usage_hashwise = function() {
	db.usage_hashwise.drop();
	print("started...");
	var usage = 0;
	previous = {};
	previous['maven_project_hash'] = "ignore";
	var count = 0;
	db.pmc_new.find().sort({
		maven_project_hash : 1
	}).forEach(function(doc) {
		if (doc['maven_project_hash'] == previous['maven_project_hash']) {
			usage = usage + 1;
		} else {
			count = count + 1;
			previous['usage'] = usage;
			db.usage_hashwise.insert(previous);
			usage = 1;
			previous = doc;
			delete previous['projects_that_imports'];
			delete previous['maven_project_group'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			delete previous['_sa_instance_state'];
		}
	});
	db.usage_hashwise.ensureIndex({
		maven_project_hash : 1
	});
	print("Done, usage_hashwise usage calculated for projects " + count);
}

var dd_usage_hashwise = function() {
	db.dd_usage_hashwise.drop();
	print("started...");
	var outer = 0;
	var inner = 0;
	db.usage_hashwise.find().forEach(function(doc) {
		outer = outer + 1;
		db.dd_hashwise.find({
			project_hash : doc['maven_project_hash']
		}).forEach(function(mdoc) {
			inner = inner + 1;
			mdoc['usage'] = doc['usage'];
			delete mdoc["_id"];
			delete mdoc["category"];
			delete mdoc["_sa_instance_state"];
			delete mdoc["priority"];
			delete mdoc["project_version"];
			delete mdoc["abbrev"];
			delete mdoc["type"];
			delete mdoc["id"];
			db.dd_usage_hashwise.insert(mdoc);
		});
	})
	db.dd_usage_hashwise.ensureIndex({
		project_name : 1
	});
	print("Done");
	print("outer: " + outer);
	print("inner: " + inner);
	print("checkout dd_usage_hashwise table, db.dd_usage_hashwise.find().limit(1).pretty()");
}

var run_calculate_hashwise_dd_usage = function() {
	dd_hashwise();
	usage_hashwise();
	dd_usage_hashwise();
}
run_calculate_hashwise_dd_usage();
