var pmc_with_fs_project_name = function(input, output) {
    db[output].drop();
    // pmc with file system project name
    print("started...");
    var count = 0;
    db[input].find().forEach(function(doc) {
        var fb_new_rows_for_given_hash = db['fb_reduced'].find({
            project_hash : doc['hash']
        }).toArray();
        if (fb_new_rows_for_given_hash.length > 0) {
            var fb_doc = fb_new_rows_for_given_hash[0];
            // there should be only one row for each maven_project_hash
            doc['project_name'] = fb_doc['project_name'];
            db[output].insert(doc);
            count = count + 1;
        }
    });
    db[output].ensureIndex({
        project_name : 1
    });
    print("done, project name appended to records: " + count);
}

var overlap = function() {
    var count = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        try {
            db.fb_reduced.find().forEach(function(fb_doc) {
                if (doc['hash'] == fb_doc['project_hash']) {
                    count = count + 1;
                    throw BreakException;
                }
            });
        } catch (e) {
            // ignore
        }
    })
    print("count: " + count);
}

var overlap2 = function() {
    var count = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        db.fb_reduced.find({
            project_hash : doc['hash']
        }).forEach(function(fb_doc) {
            print(fb_doc['project_hash']);
            count = count + 1;
        });
    });
    print("count: " + count);
}

var overlap_with_projects = function() {
    var count = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        db.projects.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
        });
    });
    print("count: " + count);
}

var overlap_with_unique_hash = function() {
    var count = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        db.unique_fs_hash.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
        });
    });
    print("count: " + count);
}

var overlap3 = function() {
    var count = 0;
    db.doi_fs_path.find().forEach(function(doc) {
        db.pmc_norm.find({
            maven_project_hash : doc['hash']
        }).forEach(function(fb_doc) {
            print(fb_doc['maven_project_hash']);
            count = count + 1;
        });
    });
    print("count: " + count);
}

var overlap4 = function() {
    var count = 0;
    var hash_with_fs = db.pmc_with_fs_project_name
            .distinct('maven_project_hash');
    var hash_all = db.pmc_norm.distinct('maven_project_hash');
    db.pmc_.find().forEach(function(doc) {
        db.pmc_with_fs_project_name.find({
            maven_project_hash : doc['hash']
        }).forEach(function(fb_doc) {
            print(fb_doc['maven_project_hash']);
            count = count + 1;
        });
    });
    print("count: " + count);
}

var overlap_with_maven_projects = function() {
    var count = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        db.hash_fspath_map.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
        });
    });
    print("count: " + count);
}

var doi_fs_path = function() {
    db.doi_fs_path.drop()
    var count = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        db.hash_fspath_map.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
            fb_doc['min'] = doc['min']
            fb_doc['max'] = doc['max']
            fb_doc['sum'] = doc['sum']
            fb_doc['mean'] = doc['mean']
            fb_doc['median'] = doc['median']
            db.doi_fs_path.insert(fb_doc)
        });
    });
    print("count: " + count);
}

var addInfo = function() {
    db.doi_usage.drop()
    var count = 0
    db.doi_fs_path.find().forEach(function(doc) {
        db.projects_using_fqn.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
            doc['projects_using_fqn'] = fb_doc['value']
            db.doi_usage.insert(doc)
        });
    })
    print("count: " + count)
}

var clean = function() {
    exp_count = 0;
    db.doi_copy_2.drop()
    db.doi_copy.find().forEach(function(doc) {
        for (property in doc) {
            if (property != "_id" && property != "_sa_instance_state") {
                var val = doc[property]
                try {
                    print(val)
                    val = val.replace(/(\r\n|\n|\r)/gm, "");
                    // val = val.replace(/^\s+|\s+$/g,'')

                } catch (e) {
                    exp_count += 1;
                }
                doc[property] = val
            }
        }
        db.doi_copy_2.insert(doc)
    })
    print("exp_count: " + exp_count)
}

var copy = function() {
    db.doi_usage.find().forEach(function(doc) {
        db.doi_copy.insert(doc)
    })
}
// group by
var map = function() {
    var component = {
        'hash' : this.hash,
        'num_hash' : 1,
        'path' : this.path,
        'min' : this.min == 'NULL' ? 'null' : Number(this.min),
        'max' : this.max == 'NULL' ? 'null' : Number(this.max),
        'mean' : this.mean == 'NULL' ? 'null' : Number(this.mean),
        'median' : Number(this.median),
        'sum' : Number(this.sum),
        'usage_files_using_fqn' : Number(this.usage_files_using_fqn
                .replace(/(\r\n|\n|\r)/gm, "")),
        'num_files_in_artifact' : this.mean == 'NULL' ? 'null' : Math
                .round(Number(this.sum) / Number(this.mean))
    };
    var key = this.path;
    print("emit: " + key)
    emit(key, component);
}
var reduce = function(key, values) {
    var component = {
        'hash' : "",
        'num_hash' : 0,
        'path' : "",
        'min' : 0,
        'max' : 0,
        'mean' : 0,
        'median' : 0,
        'sum' : 0,
        'usage_files_using_fqn' : 0,
        'num_files_in_artifact' : 0,
    };
    var min = Number.MAX_VALUE;
    var max = Number.MIN_VALUE;
    var sum = 0;
    print("***************************************");
    print("length of values: " + values.length);

    for ( var i = 0; i < values.length; i++) {
        component.num_hash += values[i].num_hash;
        component.path = values[i].path
        component.hash = values[i].hash;
        print("sum: " + values[i].sum);
        print("mean: " + values[i].mean);
        sum += Number(values[i].sum);
        print("new sum: " + sum);
        if(values[i].num_files_in_artifact!='null'){
            component.num_files_in_artifact += values[i].num_files_in_artifact;
        }
        if (values[i].min!='null') {
            if (min > Number(values[i].min)) {
                print("changing min to: " + Number(values[i].min))
                min = Number(values[i].min);
            }
            print("min: " + min);
        }
        if (values[i].max!='null') {
            if (max < Number(values[i].max)) {
                print("changing max to: " + Number(values[i].max));
                max = Number(values[i].max);
            }
            print("max: " + max);
        }
        // usage
        component.usage_files_using_fqn += Number(values[i].usage_files_using_fqn);
    }
    component.sum = sum;
    component.min = min==Number.MAX_VALUE?'null':min;
    component.max = max==Number.MIN_VALUE?'null':max;
    component.mean = component.num_files_in_artifact==0?'null':sum / component.num_files_in_artifact;
    // print(min + ", "+max +", "+sum);
    // print(component.min +", "+component.max +", "+component.sum+",
    // "+component.mean);
    return component;
}
db["dummy"].drop();
db["p_j_afferent_coupling_internal_maven_artifacts_with_fs_path_usage"].mapReduce(map, reduce, {
            out : "dummy",
            verbose : true
        })

var process_joel_data = function() {
    print("starting...")
    collections = db.getCollectionNames()
    collections
            .forEach(function(table) {
                if (table.startsWith("j_")) {
                    // this is a data collection
                    print("processing collection: " + table)
                    maven_with_path(table, "p_" + table
                            + "_maven_artifacts_with_fs_path")
                    print("path info added")

                    append_hash_usage("p_" + table
                            + "_maven_artifacts_with_fs_path", "p_" + table
                            + "_maven_artifacts_with_fs_path" + "_usage")
                    print("artifact usage added")
                    component_statistics_table_name = "p_" + "component_usage_"
                            + table;
                    db[component_statistics_table_name].drop();
                    result = db["p_" + table + "_maven_artifacts_with_fs_path"
                            + "_usage"].mapReduce(map, reduce, {
                        out : component_statistics_table_name
                    })
                    print("component usage stats created: "
                            + component_statistics_table_name)
                }
                // print(table)
            })
    print("done")
}

var remove_joel_data = function() {
    collections = db.getCollectionNames()
    collections.forEach(function(table) {
        if (table.startsWith("j_")) {
            print("droping " + table)
            db[table].drop()
        }
        if (table.startsWith("component_usage_j_")) {
            print("droping " + table)
            db[table].drop()
        }
    });
}

var maven_with_path = function(table, outTable) {
    db[outTable].drop()
    var count = 0;
    db[table].find().forEach(function(doc) {
        db.hash_fspath_map.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            doc['path'] = fb_doc['path']
            db[outTable].insert(doc) // table+"_usage"
            count = count + 1;
        });
    });
    print("count: " + count);
}

// map-reduce job
// append to one collection

// this is not aggregated component's usage.
var append_hash_usage = function(table, outTable) {
    var usage_file = "files_using_fqn"
    db[outTable].drop()
    var count = 0;
    db[table].find().forEach(function(doc) {
        db[usage_file].find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
            doc['usage_'+usage_file] = fb_doc['value']
            db[outTable].insert(doc)
        });
    })
}

// db.components_maven_usage.drop();
// result = db.doi_copy_2.mapReduce(map,reduce,"components_maven_usage")

db.doi_fs_path.aggregate({
    $group : {
        _id : "$path",
        count : {
            $sum : 1
        }
    }
})
// hash_fspath_map

var overlap_with_other_projects = function() {
    var count = 0;
    var type = {};
    type['SYSTEM'] = 0;
    type['JAVA_LIBRARY'] = 0;
    type['CRAWLED'] = 0;
    type['JAR'] = 0;
    db.depth_of_inheritance.find().forEach(function(doc) {
        db.other_projects.find({
            hash : doc['hash']
        }).forEach(function(fb_doc) {
            // print(fb_doc['hash']);
            count = count + 1;
            type[fb_doc['project_type']] = type[fb_doc['project_type']] + 1;
        });
    });
    print("count: " + count);
    for ( var property in type) {
        print(property + " : " + type[property]);
    }
}

var subsetProject = function() {
    db.projects.find().forEach(function(doc) {
        if (doc['project_type'] == 'MAVEN') {
            db.maven_projects.insert(doc)
        } else {
            db.other_projects.insert(doc)
        }
    });
}

// print("mongoexport --csv --fieldFile fields.txt --out
// p_component_usage_j_depth_of_inheritance_tree_internal_cc.csv --db "
// + "utilization --collection
// p_component_usage_j_depth_of_inheritance_tree_internal_cc");

var run = function() {
    pmc_with_fs_project_name('depth_of_inheritance', 'doi_withfs');
}
