var dd_maven_group = function() {
	db.dd_maven_group.drop();
	sum_total_bug = 0
	sum_total_size = 0;
	defect_density = 0;
	previous = "";
	db.dd_fb_pmc.find().sort({
		maven_project_group : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	db.dd_fb_pmc.find().sort({
		maven_project_group : 1
	}).forEach(function(doc) {
		if (previous['maven_project_group'] != doc['maven_project_group']) {
			defect_density = (1000 * sum_total_bug) / sum_total_size;
			previous['sum_total_bug'] = sum_total_bug;
			previous['sum_total_size'] = sum_total_size;
			previous['defect_density'] = defect_density;
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			db.dd_maven_group.insert(previous);
			sum_total_bug = doc['sum_total_bug'];
			sum_total_size = doc['sum_total_size'];
			previous = doc;
		}
		sum_total_bug = sum_total_bug + doc['sum_total_bug'];
		sum_total_size = sum_total_size + doc['sum_total_size'];
	});
	db.dd_maven_group.ensureIndex({
		maven_project_group : 1
	});
}

var usage_maven_group = function() {
	db.maven_usage_new.drop();
	print("started...");
	var usage = 0;
	previous = {};
	previous['maven_project_group'] = "ignore";
	var count = 0;
	db.pmc_new.find().sort({
		maven_project_group : 1
	}).forEach(function(doc) {
		if (doc['maven_project_group'] == previous['maven_project_group']) {
			usage = usage + 1;
		} else {
			count = count + 1;
			previous['usage'] = usage;
			db.maven_usage_new.insert(previous);
			usage = 1;
			previous = doc;
			delete previous['projects_that_imports'];
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			delete previous['_sa_instance_state'];
		}
	});
	db.maven_usage_new.ensureIndex({
		maven_project_group : 1
	});
	print("Done, maven_usage_new usage calculated for projects " + count);
}

db.delme_pmc_fs_pn.ensureIndex({
    project_name : 1
});
var usage_maven_group_test = function() {
    db.maven_usage_new_test.drop();
    print("started...");
    var usage = 0;
    previous = {};
    previous['project_name'] = "ignore";
    var count = 0;
    db.delme_pmc_fs_pn.find().sort({
        project_name : 1
    }).forEach(function(doc) {
        if (doc['project_name'] == previous['project_name']) {
            usage = usage + 1;
        } else {
            count = count + 1;
            previous['usage'] = usage;
            db.maven_usage_new_test.insert(previous);
            usage = 1;
            previous = doc;
            delete previous['projects_that_imports'];
            delete previous['maven_project_hash'];
            //delete previous['maven_project_id'];
            //delete previous['project_name'];
            delete previous['_sa_instance_state'];
        }
    });
    db.maven_usage_new_test.ensureIndex({
        maven_project_group : 1
    });
    print("Done, maven_usage_new usage calculated for projects " + count);
}

var getDistinct = function(){
	var visited = {};
	db.pmc_new.find().forEach(function(doc){
		if((doc['projects_that_imports']+":"+doc['maven_project_id']) in visited){
			//ignore
		}else{
			db.pmc_new_distinct_pair.insert(doc);
			visited[doc['projects_that_imports']+":"+doc['maven_project_id']]=1;
		}
	});
}

var update_stats_new = function() {
	db.dd_usage_new.drop();
	print("started...");
	var outer = 0;
	var inner = 0;
	db.maven_usage_new.find().forEach(function(doc) {
		outer = outer + 1;
		db.dd_maven_group.find({
			maven_project_group : doc['maven_project_group']
		}).forEach(function(mdoc) {
			inner = inner + 1;
			mdoc['usage'] = doc['usage'];
			db.dd_usage_new.insert(mdoc);
		});
	})
	db.dd_usage_new.ensureIndex({
		project_name : 1
	});
	db.dd_usage_new.ensureIndex({
		maven_project_group : 1
	});
	print("Done");
	print("outer: " + outer);
	print("inner: " + inner);
	print("checkout dd_usage_new table, db.dd_usage_new.find().pretty()");
}



var run_calculate_maven_group_dd_usage = function() {
	dd_maven_group();
	usage_maven_group();
	update_stats_new();
}
run_calculate_maven_group_dd_usage();

//mongoexport --csv -o maven_group_stats.csv -d utilization -c dd_usage_new -f _id,usage,maven_project_group,maven_project_name,sum_total_bug,sum_total_size,defect_density
