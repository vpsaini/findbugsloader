// create a new collection that will only have these 6 categories
var fb_selected_categories = function() {
	db.fb_selected_categories.drop()
	db.fb_new.find({
		category : {
			$in :[
				"MALICIOUS_CODE",
				"PERFORMANCE",
				"CORRECTNESS",
				"EXPERIMENTAL",
				"MT_CORRECTNESS",
				"SECURITY"
			]
		}
	}).forEach(function(doc) {
		db.fb_selected_categories.insert(doc);
	});
	db.fb_selected_categories.ensureIndex({
		project_hash : 1
	});
	db.fb_selected_categories.ensureIndex({
		project_name : 1
	});
};

// create a new collection that will have unique project_hash
// project_bug_count will have the total bug count of this project
var fb_reduced_selected_categories = function() {
	previous = "";
	db.fb_selected_categories.find().sort({
		project_hash : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	var bug_count = 0;
	db.fb_selected_categories.find().sort({
		project_hash : 1
	}).forEach(function(doc) {
		if (doc['project_hash'] == previous['project_hash']) {
			// add the count
			bug_count = bug_count + 1;
			
		} else {
			previous['project_bug_count_reduced_category'] = bug_count;
			db.fb_reduced_selected_categories.insert(previous);
			previous = doc;
			bug_count = 1;
		}
	});
	db.fb_reduced_selected_categories.ensureIndex({
		project_hash : 1
	});
	db.fb_reduced_selected_categories.ensureIndex({
		project_name : 1
	});
};
// create a collection that will have defect density calculated on the basis of file_system_project_name
var dd_fb_pmc_reduced = function() {
	db.dd_fb_pmc_reduced.drop();
	count = 0;
	fb_doc_count = 0;
	fb_count = 0;
	visited_projects = {};
	db.pmc_norm.find().forEach(function(doc) {
		try {
			
			db.fb_reduced_selected_categories.find({
				project_hash : doc['maven_project_hash']
			}).forEach(function(fb_doc) {
				if (fb_doc['project_name'] in visited_projects) {
					throw BreakException;
				}
				visited_projects[fb_doc['project_name']] = 1;
				fb_doc_count = fb_doc_count + 1;
				// there should be only one row for each maven_project_hash
				// print(fb_doc['project_name']);
				sum_total_bug = 0;
				sum_total_size = 0;
				defect_density = 0;
				version_count =0;
				db.fb_reduced_selected_categories.find({
					project_name : fb_doc['project_name']
				}).forEach(function(fb) {
					fb_count = fb_count + 1;
					version_count = version_count +1;
					// iterate on all the rows with this project_name
					sum_total_bug = sum_total_bug + fb['project_bug_count_reduced_category'];
					sum_total_size = sum_total_size + fb['project_size'];
				});
				if (0 != sum_total_size) {
					defect_density = (1000 * sum_total_bug) / sum_total_size
				}
				doc['project_name'] = fb_doc['project_name'];
				doc['sum_total_bug'] = sum_total_bug;
				doc['sum_total_size'] = sum_total_size;
				doc['defect_density'] = defect_density;
				doc['version_count'] = version_count;
				db.dd_fb_pmc_reduced.insert(doc);
			})
		} catch (e) {
			// print("caught yay");
		}
		count = count + 1;
	});
	db.dd_fb_pmc_reduced.ensureIndex({
		project_name : 1
	});
	print('done');
	print("doc count is " + count);
	print("fb_count count is " + fb_doc_count);
	print("fb_count" + fb_count);
	db.dd_fb_pmc_reduced.ensureIndex({
		maven_project_group : 1
	});
};

var pmc_with_fs_project_name_reduced_categories = function() {
	db.pmc_with_fs_project_name_reduced_categories.drop();
	// pmc with file system project name
	print("started...");
	var count = 0;
	db.pmc_new.find().forEach(function(doc) {
		db.fb_reduced_selected_categories.find({
			project_hash : doc['maven_project_hash']
		}).forEach(function(fb_doc) {
			// there should be only one row for each maven_project_hash
			doc['project_name'] = fb_doc['project_name'];
			db.pmc_with_fs_project_name_reduced_categories.insert(doc);
			count = count + 1;
		});
	});
	db.pmc_with_fs_project_name_reduced_categories.ensureIndex({
		project_name : 1
	});
	db.pmc_with_fs_project_name_reduced_categories.ensureIndex({
		maven_project_group : 1
	});
	print("done, project name appended to records: " + count);
}
var maven_usage_reduced_categories = function() {
	db.maven_usage_reduced_categories.drop();
	print("started...");
	var usage = 0;
	previous = {};
	previous['project_name'] = "ignore";
	var count = 0;
	db.pmc_with_fs_project_name_reduced_categories.find().sort({
		project_name : 1
	}).forEach(function(doc) {
		if (doc['project_name'] == previous['project_name']) {
			usage = usage + 1;
		} else {
			count = count + 1;
			previous['usage'] = usage;
			db.maven_usage_reduced_categories.insert(previous);
			usage = 1;
			previous = doc;
			delete previous['projects_that_imports'];
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
		}
	});
	db.maven_usage_reduced_categories.ensureIndex({
		project_name : 1
	});
	print("Done, maven usage calculated for projects " + count);
}
var dd_maven_group_reduced = function() {
	db.dd_maven_group_reduced.drop();
	sum_total_bug = 0
	sum_total_size = 0;
	defect_density = 0;
	previous = "";
	db.dd_fb_pmc_reduced.find().sort({
		maven_project_group : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	db.dd_fb_pmc_reduced.find().sort({
		maven_project_group : 1
	}).forEach(function(doc) {
		if (previous['maven_project_group'] != doc['maven_project_group']) {
			defect_density = (1000 * sum_total_bug) / sum_total_size;
			previous['sum_total_bug'] = sum_total_bug;
			previous['sum_total_size'] = sum_total_size;
			previous['defect_density'] = defect_density;
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			db.dd_maven_group_reduced.insert(previous);
			db.dd_maven_group.insert(previous);
			sum_total_bug = doc['sum_total_bug'];
			sum_total_size = doc['sum_total_size'];
			previous = doc;
		}
		sum_total_bug = sum_total_bug + doc['sum_total_bug'];
		sum_total_size = sum_total_size + doc['sum_total_size'];
	});
	db.dd_maven_group_reduced.ensureIndex({
		maven_project_group : 1
	});
};
var usage_maven_group = function() {
	db.maven_usage_new.drop();
	print("started...");
	var usage = 0;
	previous = {};
	previous['maven_project_group'] = "ignore";
	var count = 0;
	db.pmc_new.find().sort({
		maven_project_group : 1
	}).forEach(function(doc) {
		if (doc['maven_project_group'] == previous['maven_project_group']) {
			usage = usage + 1;
		} else {
			count = count + 1;
			previous['usage'] = usage;
			db.maven_usage_new.insert(previous);
			usage = 1;
			previous = doc;
			delete previous['projects_that_imports'];
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			delete previous['_sa_instance_state'];
		}
	});
	db.maven_usage_new.ensureIndex({
		maven_project_group : 1
	});
	print("Done, maven_usage_new usage calculated for projects " + count);
}
var dd_usage_reduced_categories = function() {
	db.dd_usage_reduced_categories.drop();
	print("started...");
	var outer = 0;
	var inner = 0;
	db.maven_usage_new.find().forEach(function(doc) {
		outer = outer + 1;
		db.dd_maven_group_reduced.find({
			maven_project_group : doc['maven_project_group']
		}).forEach(function(mdoc) {
			inner = inner + 1;
			mdoc['usage'] = doc['usage'];
			db.dd_usage_reduced_categories.insert(mdoc);
		});
	})
	db.dd_usage_reduced_categories.ensureIndex({
		project_name : 1
	});
	db.dd_usage_reduced_categories.ensureIndex({
		maven_project_group : 1
	});
	print("Done");
	print("outer: " + outer);
	print("inner: " + inner);
	print("checkout dd_usage_reduced_categories table, db.dd_usage_reduced_categories.find().pretty()");
};

var inlcude_remaining_from_maven_group_wise = function() {
	db.dd_usage_new.find().forEach(function(doc) {
		found = 0;
		db.dd_usage_reduced_categories.find({
			maven_project_group : doc['maven_project_group']
		}).forEach(function() {
			found = found + 1;
		});
		if (found == 0) {
			doc['defect_density'] = 0;
			doc['sum_total_bug'] = 0;
			db.dd_usage_reduced_categories.insert(doc)

		}
	})
}

var test = function() {
	db.dd_usage_reduced_categories.find().forEach(function(doc) {
		found = 0;
		db.dd_usage_new.find({
			maven_project_group : doc['maven_project_group']
		}).forEach(function() {
			found = found + 1;
		});
		if (found == 0) {
			print(doc['maven_project_group'])
		}
	})
}

var runme = function() {
	// fb_selected_categories();
	fb_reduced_selected_categories();
	dd_fb_pmc_reduced();
	dd_maven_group_reduced();
	usage_maven_group();
	dd_usage_reduced_categories();
};
runme();

// mongoexport --csv -o dd_usage_reduced_categories.csv -d utilization -c
// dd_usage_reduced_categories -f
// _id,usage,maven_project_group,maven_project_name,sum_total_bug,sum_total_size,defect_density
