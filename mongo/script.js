// function to create fb_reduced
// this will have only one row for one hash. 
// we dont need multiple rows for one hash as every such row contains the version's total bug count and total size

var reduced_fb = function() {
	previous = "";
	db.fb_new.find().sort({
		project_hash : 1
	}).forEach(function(doc) {
		if (doc['project_hash'] != previous) {
			db.fb_reduced.insert(doc);
			previous = doc['project_hash'];
		}
	});
	db.fb_reduced.ensureIndex({
		project_hash : 1
	});
	db.fb_reduced.ensureIndex({
		project_name : 1
	});
};

// for each hash in pmc_norm
// get project name for this hash from fb_reduced.
// get all rows for this project_name
// s = sum (project_size) and bc = sum (project_bug_count)
// calculate defect_density = (bc*1000)/s

/*
 * "_id" : ObjectId("51bea228f24a8a2db1fb4a63"), "usage" : 2101,
 * "maven_project_hash" : "a22958b18399506d188d0b4c211ef27d", "maven_project_id" :
 * 96718, "maven_project_name" : "org.apache.servicemix.bundles.gwt-user",
 * "maven_project_group" : "org.apache.servicemix.bundles", "project_name" :
 * "/home/sourcerer/processedbugprofile/org/apache/servicemix/servicemix-xmpp",
 * "sum_total_bug" : 251083, "sum_total_size" : 26667432, "defect_density" :
 */
var stats = function() {
	db.dd_fb_pmc.drop();
	count = 0;
	fb_doc_count = 0;
	fb_count = 0;
	visited_projects = {};
	db.pmc_norm.find().forEach(function(doc) {
		// print(doc['maven_project_hash']);
		try {
			db.fb_reduced.find({
				project_hash : doc['maven_project_hash']
			}).forEach(function(fb_doc) {
				if (fb_doc['project_name'] in visited_projects) {
					throw BreakException;
				}
				visited_projects[fb_doc['project_name']] = 1;
				fb_doc_count = fb_doc_count + 1;
				// there should be only one row for each maven_project_hash
				// print(fb_doc['project_name']);
				sum_total_bug = 0
				sum_total_size = 0;
				defect_density = 0;
				db.fb_reduced.find({
					project_name : fb_doc['project_name']
				}).forEach(function(fb) {
					fb_count = fb_count + 1;
					// iterate on all the rows with this project_name
					sum_total_bug = sum_total_bug + fb['project_bug_count'];
					sum_total_size = sum_total_size + fb['project_size'];
				});
				if (0 != sum_total_size) {
					defect_density = (1000 * sum_total_bug) / sum_total_size
				}
				doc['project_name'] = fb_doc['project_name'];
				doc['sum_total_bug'] = sum_total_bug;
				doc['sum_total_size'] = sum_total_size;
				doc['defect_density'] = defect_density;
				db.dd_fb_pmc.insert(doc);
			})
		} catch (e) {
			// print("caught yay");
		}
		count = count + 1;
	});
	db.dd_fb_pmc.ensureIndex({
		project_name : 1
	});
	print('done');
	print("doc count is " + count);
	print("fb_count count is " + fb_doc_count);
	print("fb_count" + fb_count);
	db.dd_fb_pmc.ensureIndex({
		maven_project_group : 1
	});
}
var dd_maven_group = function() {
	db.dd_maven_group.drop();
	sum_total_bug = 0
	sum_total_size = 0;
	defect_density = 0;
	previous = "";
	db.dd_fb_pmc.find().sort({
		maven_project_group : 1
	}).limit(1).forEach(function(doc) {
		previous = doc;
	});
	db.dd_fb_pmc.find().sort({
		maven_project_group : 1
	}).forEach(function(doc) {
		if (previous['maven_project_group'] != doc['maven_project_group']) {
			defect_density = (1000 * sum_total_bug) / sum_total_size;
			previous['sum_total_bug'] = sum_total_bug;
			previous['sum_total_size'] = sum_total_size;
			previous['defect_density'] = defect_density;
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			db.dd_maven_group.insert(previous);
			previous = doc;
		}
		sum_total_bug = sum_total_bug + doc['sum_total_bug'];
		sum_total_size = sum_total_size + doc['sum_total_size'];
	});
	db.dd_maven_group.ensureIndex({
		maven_project_group : 1
	});
}

var pmc_with_fs_project_name = function() {
	db.pmc_with_fs_project_name.drop();
	// pmc with file system project name
	print("started...");
	var count = 0;
	db.pmc_new.find().forEach(function(doc) {
		db.fb_reduced.find({
			project_hash : doc['maven_project_hash']
		}).forEach(function(fb_doc) {
			// there should be only one row for each maven_project_hash
			doc['project_name'] = fb_doc['project_name'];
			db.pmc_with_fs_project_name.insert(doc);
			count = count + 1;
		});
	});
	db.pmc_with_fs_project_name.ensureIndex({
		project_name : 1
	});
	db.pmc_with_fs_project_name.ensureIndex({
		maven_project_group : 1
	});
	print("done, project name appended to records: " + count);

}

var usage = function() {
	db.maven_usage.drop();
	print("started...");
	var usage = 0;
	previous = {};
	previous['project_name'] = "ignore";
	var count = 0;
	db.pmc_with_fs_project_name.find().sort({
		project_name : 1
	}).forEach(function(doc) {
		if (doc['project_name'] == previous['project_name']) {
			usage = usage + 1;
		} else {
			count = count + 1;
			previous['usage'] = usage;
			db.maven_usage.insert(previous);
			usage = 1;
			previous = doc;
			delete previous['projects_that_imports'];
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
		}
	});
	db.maven_usage.ensureIndex({
		project_name : 1
	});
	print("Done, maven usage calculated for projects " + count);
}

db.pmc_with_fs_project_name.ensureIndex({
	maven_project_group : 1
});
var usage_maven_group = function() {
	db.maven_usage_new.drop();
	print("started...");
	var usage = 0;
	previous = {};
	previous['maven_project_group'] = "ignore";
	var count = 0;
	db.pmc_new.find().sort({
		maven_project_group : 1
	}).forEach(function(doc) {
		if (doc['maven_project_group'] == previous['maven_project_group']) {
			usage = usage + 1;
		} else {
			count = count + 1;
			previous['usage'] = usage;
			db.maven_usage_new.insert(previous);
			usage = 1;
			previous = doc;
			delete previous['projects_that_imports'];
			delete previous['maven_project_hash'];
			delete previous['maven_project_id'];
			delete previous['project_name'];
			delete previous['_sa_instance_state'];
		}
	});
	db.maven_usage_new.ensureIndex({
		maven_project_group : 1
	});
	print("Done, maven_usage_new usage calculated for projects " + count);
}

var update_stats = function() {
	db.dd_usage.drop();
	print("started...");
	var outer = 0;
	var inner = 0;
	db.maven_usage.find().forEach(function(doc) {
		outer = outer + 1;
		db.dd_fb_pmc.find({
			project_name : doc['project_name']
		}).forEach(function(mdoc) {
			inner = inner + 1;
			mdoc['usage'] = doc['usage'];
			db.dd_usage.insert(mdoc);
		});
	})
	db.dd_usage.ensureIndex({
		project_name : 1
	});
	print("Done");
	print("outer: " + outer);
	print("inner: " + inner);
	print("checkout dd_usage table, db.dd_usage.find().pretty()");
}

var update_stats_new = function() {
	db.dd_usage_new.drop();
	print("started...");
	var outer = 0;
	var inner = 0;
	db.maven_usage_new.find().forEach(function(doc) {
		outer = outer + 1;
		db.dd_maven_group.find({
			maven_project_group : doc['maven_project_group']
		}).forEach(function(mdoc) {
			inner = inner + 1;
			mdoc['usage'] = doc['usage'];
			db.dd_usage_new.insert(mdoc);
		});
	})
	db.dd_usage_new.ensureIndex({
		project_name : 1
	});
	print("Done");
	print("outer: " + outer);
	print("inner: " + inner);
	print("checkout dd_usage_new table, db.dd_usage_new.find().pretty()");
}

var runme = function() {
	reduced_fb();
	stats();// calculates the defect density
	pmc_with_fs_project_name();
	usage();
	update_stats(); 
}
runme();
