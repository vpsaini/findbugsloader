CREATE TABLE `find_bugs_withpg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `abbrev` varchar(50) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `project_hash` varchar(32) NOT NULL,
  `project_name` varchar(2000) NOT NULL,
  `project_version` varchar(200) NOT NULL,
  `project_size` int(11) NOT NULL,
  `project_bug_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_hash_index` (`project_hash`)
) ENGINE=MyISAM AUTO_INCREMENT=6832236 DEFAULT CHARSET=latin1