create table fb_pmc_join as (
select 
    fb.id as fb_id,
    fb.type as fb_type,
    fb.priority as fb_priority,
    fb.abbrev as fb_abbrev,
    fb.category as fb_category,
    pmc . *
from
    project_to_maven_component_map pmc
        inner join
    find_bugs fb ON (fb.project_hash = pmc.maven_project_hash)
)
