create table maven_component_utilization as (
SELECT 
    e2.entity_id as entity_id,
    e2.fqn as fqn,
    e2.project_id as maven_projects,
    i.file_id as file_id,
    i.project_id as projects_that_imports,
	p1.name as maven_project_name ,
	p1.groop as maven_project_group
FROM
    imports i
        inner join
    entities e ON (e.entity_id = i.eid)
        inner join
    entities e2 ON (e2.fqn = e.fqn)
        inner join
    projects p2 ON (p2.project_id = i.project_id
        and p2.project_type <> 'MAVEN')
        inner join
    projects p1 ON (p1.project_id = e2.project_id
        and p1.project_type = 'MAVEN'))