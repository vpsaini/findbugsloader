create table project_to_maven_component_map as (
select 
    projects_that_imports,
    maven_project_hash,
    maven_projects as maven_project_id,
    maven_project_name,
    maven_project_group
from
    maven_component_utilization_imp
group by projects_that_imports , maven_project_hash , maven_projects , maven_project_name , maven_project_group
);