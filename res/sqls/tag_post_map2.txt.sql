/*
-- Query: explain select 
    fb.id as fb_id,
    fb.type as fb_type,
    fb.priority as fb_priority,
    fb.abbrev as fb_abbrev,
    fb.category as fb_category,
    pmc . *
from
    find_bugs fb
        inner join
    project_to_maven_component_map pmc ON (fb.project_hash = pmc.maven_project_hash)
-- Date: 2013-06-16 16:39
*/
INSERT INTO `pmc` (`id`,`select_type`,`table`,`type`,`possible_keys`,`key`,`key_len`,`ref`,`rows`,`Extra`) VALUES (1,'SIMPLE','fb','ALL','project_hash_index',NULL,NULL,NULL,6832235,'');
INSERT INTO `pmc` (`id`,`select_type`,`table`,`type`,`possible_keys`,`key`,`key_len`,`ref`,`rows`,`Extra`) VALUES (1,'SIMPLE','pmc','ref','maven_project_hash_index','maven_project_hash_index','35','utilization.fb.project_hash',607,'Using where');
