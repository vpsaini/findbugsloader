select 
    distinct i.eid
from
    imports i
        inner join
    (select 
        e.entity_id as entity_id
    from
        projects p
    inner join entities e ON (e.project_id = p.project_id
        and p.project_type = 'MAVEN'
        and e.fqn = 'org.apache.http.client.methods.HttpPost')) as internal ON (i.eid = entity_id)

