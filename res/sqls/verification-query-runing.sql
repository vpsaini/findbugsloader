select 
    entity_id,
	maven_projects,
fqn,
projects_that_imports
from
    (select 
        entity_id,
            fqn,
            maven_projects,
            file_id,
            projects_that_imports
    from
        (SELECT 
        e2.entity_id as entity_id,
            e2.fqn as fqn,
            e2.project_id as maven_projects,
            i2.file_id as file_id,
            i2.project_id as projects_that_imports
    FROM
        imports i
    inner join entities e ON (e.entity_id = i.eid and i.eid = 17253057)
    inner join entities e2 ON (e2.fqn = e.fqn)
    inner join imports i2 ON (i2.eid = e2.entity_id)
    inner join projects p2 ON (p2.project_id = i2.project_id
        and p2.project_type <> 'MAVEN')) as internal
    inner join projects p ON (p.project_type = 'MAVEN'
        and p.project_id = internal.maven_projects)) as external
	group by entity_id,
	maven_projects,
fqn,
projects_that_imports