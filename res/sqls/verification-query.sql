SELECT 
    e2.entity_id,
    e2.fqn,
    e2.project_id as maven_projects,
    i2.file_id,
    i2.project_id as projects_that_imports
,count(*) as count
FROM
    imports i
        inner join
    entities e ON (e.entity_id = i.eid
        and i.eid = 17253057)
        inner join
    entities e2 ON (e2.fqn = e.fqn)
        inner join
    projects p ON (p.project_id = e2.project_id
        and p.project_type = 'MAVEN')
        inner join
    imports i2 ON (i2.eid = e2.entity_id)
	group by e2.entity_id, e2.fqn, e2.project_id, i2.file_id, i2.project_id
order by e2.entity_id


