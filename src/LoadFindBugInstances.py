'''
Created on Jun 10, 2013

@author: vaibhavsaini
'''
import importlib
import os
import re
import sys
from xml.dom.minidom import *

from DbConnect import DbConnect
from Models import FindBugs, ProjectToMavenComponentMap, \
    source_projects_complete_info, ProjectHash, Projects, HashToFSpath
from fileinput import filename


class LoadBugInstances():
    
    def __init__(self, config):
        self.dbConfig = config['dbConfig']
        self.source = config['osConfig']['source']
        self.dest = config['osConfig']['dest']
        self.dbConnect = DbConnect(self.dbConfig)
        Session, self.engine = self.dbConnect.openConnection()
        self.session = Session()
    def clean(self):
        print "cleaning"
        os.system("rm -rf {}".format(self.dest))
        print "cleaning done"
    
    def copy(self):
        """take a root directory
            take destination directory
           copy all from root including subdirs to the destination"""
        print "copying files"
        os.system("cp -r {0} {1}".format(self.source, self.dest))
        print "copying done"
    
    def locateAndProcessZip(self):
        print "processing zip started.."
        """go to the dir where output.zip is present
        unzip output.zip"""
        for root, subFolders, files in os.walk(self.dest):
            #print "root : {0}".format(root)
            for file in files:
                fileName, fileExtension = os.path.splitext(file)
                if fileExtension == '.properties':
                    if fileName == "jar":
                        #print "found", fileName
                        #cmd = "unzip -o {0}/{1}{2} -d {0}/".format(root, fileName, fileExtension)
                        #print "cmd", cmd
                        #os.system(cmd)
                        # delete the zip file
                        #cmd = "rm -rf {0}/{1}{2}".format(root, fileName, fileExtension)
                        #os.system(cmd)
                        try:
                            project_hash = self.readProperties(root)
                            #print "project_hash : ", project_hash
                                #bugInstances = self.readBugInstances(root, project_hash)
                            hashToPath = HashToFSpath()
                            hashToPath.hash=project_hash;
                            hashToPath.path = self.getFileSystemPath(root, project_hash).strip();
                            try:
                                self.session.add(hashToPath)
                                self.session.commit();
                            except:
                                print "ignore hash, error in saving to db ", project_hash, hashToPath.path
                                print "sys.exc_info:", sys.exc_info()[0]
                        except:
                            print "ignore, properties file not found",
                            print "sys.exc_info:", sys.exc_info()[0]

    def getFileSystemPath(self, folder, project_hash):
        version = os.path.basename(os.path.normpath(folder))
        parts = folder.split('/')
        fs_parts = parts[0:-1];
        fs_path = '/'.join(fs_parts);
        return fs_path
        
    def loadHash(self):
            print "processing zip started.."
            """go to the dir where output.zip is present
            unzip output.zip"""
            count =0;
            for root, subFolders, files in os.walk(self.dest):
                print "root : {0}".format(root)
                for file in files:
                    fileName, fileExtension = os.path.splitext(file)
                    if fileExtension == '.properties' and fileName == "jar":
                        try:
                            project_hash = self.readProperties(root)
                            parts = root.split('/')
                            group_parts = parts[0:-1];
                            fileSystemPath = '/'.join(group_parts);
                            version = os.path.basename(os.path.normpath(root))
                            projectHash = ProjectHash()
                            projectHash.project_fs_path = fileSystemPath
                            projectHash.project_hash = project_hash
                            projectHash.project_version = version
                            self.session.add(projectHash)
                            self.session.commit();
                            count = count + 1
                            if 1000 % count == 0:
                                print "rows inserted: {0}".format(count)
                        except:
                            print "ignore, properties file not found",
                            print "sys.exc_info:", sys.exc_info()[0]

    
    def checkIfVersionStr(self, inputStr):
        pattern = "^(\d+\.)*(\d)*$"
        expression = re.compile(pattern)
        result = expression.match(inputStr)
        return result
        
    def readProperties(self, folder):
        """read jar.properties and get project hash"""
        filename = '{0}/jar.properties'.format(folder)
        try:
            f = open(filename, 'r')
            lines = f.readlines()
            for line in lines:
                if line.startswith('hash'):
                    project_hash = line.split("=")[1].strip()
                    return project_hash
        except:
            raise Exception("jar.properties not found")
        finally:
            f.close()
            
    def readBugInstances(self, folder, project_hash):
        """ read and parse findbug.xml"""
        filename = '{0}/findbugs.xml'.format(folder)
        version = os.path.basename(os.path.normpath(folder))
        parts = folder.split('/')
        group_parts = parts[0:-1];
        group = '/'.join(group_parts);
       # print "findbugs.xml:  ", filename
       # print "version: ", version
        try:
            f = open(filename, 'r')
            data = f.read()
            dom = parseString(data)
            findbugSummary = dom.getElementsByTagName("FindBugsSummary")[0]
            total_size = findbugSummary.getAttribute("total_size")
            total_bugs = findbugSummary.getAttribute("total_bugs")
            bugInstanceTags = dom.getElementsByTagName("BugInstance")
            bugInstances = []
            if total_bugs == 0:
                bugIns = FindBugs()
                bugIns.type = "NA"
                bugIns.priority = "NA"
                bugIns.abbrev = "NA"
                bugIns.category = "NA"
                bugIns.project_hash = project_hash
                bugIns.project_name = group
                bugIns.project_version = version
                bugIns.project_size = total_size
                bugIns.project_bug_count = total_bugs
                bugInstances.append(bugIns)
            else:
                for bugInstanceTag in bugInstanceTags:
                    type = bugInstanceTag.getAttribute("type")
                    priority = bugInstanceTag.getAttribute("priority")
                    abbrev = bugInstanceTag.getAttribute("abbrev")
                    category = bugInstanceTag.getAttribute("category")
                    bugIns = FindBugs()
                    bugIns.type = type
                    bugIns.priority = priority
                    bugIns.abbrev = abbrev
                    bugIns.category = category
                    bugIns.project_hash = project_hash
                    bugIns.project_name = group
                    bugIns.project_version = version
                    bugIns.project_size = total_size
                    bugIns.project_bug_count = total_bugs
                    bugInstances.append(bugIns)
            return bugInstances
        except:
            print "findbugs.xml not found!", sys.exc_info()[0], filename
            raise Exception("findbugs.xml not found!")
        finally:
            f.close()

    def readData(self, start, end):
        rows = self.session.query(HashToFSpath)[start:end]
        return rows

    def readPMCData(self, start, end):
        rows = self.session.query(ProjectToMavenComponentMap)[start:end]
        return rows

if __name__ == '__main__':
    
    dbConfig = { 'host': 'snake.ics.uci.edu',
                         'user': 'sourcerer',
                         'pass': 'tyl0n4pi',
                         'db': 'utilization'}
    # osConfig = {"source": "/home/sourcerer/extracted/repo/asm/jars/maven",
    #            "dest": "/home/sourcerer/processedbugprofile"}
    # osConfig = {"source": "/home/sourcerer/delme",
    #             "dest": "/home/sourcerer/processedbugprofile"}
    # osConfig = {"source": "/Users/vaibhavsaini/Documents/workspace/input",
     #                  "dest": "/Users/vaibhavsaini/Documents/workspace/output"}
    osConfig = {"source": "/home/sourcerer/repo/jars/maven",
                "dest": "/home/sourcerer/repo/jars/maven"}
    config = {"dbConfig":dbConfig,
              "osConfig": osConfig}
    lbi = LoadBugInstances(config);
    #lbi.clean()
    # var = raw_input("folder deleted, press enter to continue ")
    #lbi.copy()
    print "copy done"
    lbi.locateAndProcessZip()
    print "processing zip done. check the db now"
# ##    
"""
    try:
        s = commands.getoutput('ps aux | grep srch2-search-server')
        print "s is :  ", s
        stat = s.split()
        print "stat[1] is ", stat[1]
        os.kill(int(stat[1]), signal.SIGUSR1)
    except: 
        s = commands.getoutput("ps -A | grep -m1 srch2-search-server | awk '{print $1}'")
        print "s is :  ", s
        a = s.split()
        cmd = "kill -9 {0}".format(a[-1])
        os.system(cmd)
    print '=============================='
    6575827
    """
    # TODO: delete output.zip
    # read and parse findbug.xml
    # create instances of FindBugs for each buginstance in above xml 
    # populate each findbug instance with required values including project_id and hash
    # save all the instances
    
    # repeat it for all the output.zip files 
# for i in `find ./deletme_extracted_maven/ -type f -name output.zip`; do 
#   unzip -l $i | grep "findbugs.xml" >> count_fbxml.txt; 
# done
