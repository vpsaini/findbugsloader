'''
Created on Feb 21, 2013

@author: vaibhavsaini
'''
from sqlalchemy.schema import Column, Sequence
from sqlalchemy.types import *
from Base import *

class FindBugs(Base):
    '''
    classdocs
    '''
    __tablename__ = 'find_bugs_withpg'
    id = Column(Integer, primary_key=True)
    type = Column(String(200))
    priority = Column(Integer)
    abbrev = Column(String(50))
    category = Column(String(50))
    project_hash = Column(String(32))
    project_name = Column(String(2000))
    project_version = Column(String(200))
    project_size = Column(Integer)
    project_bug_count = Column(Integer)

    def getVars(self):
        return vars(self)
    
    def __repr__(self):
        return "<Post (%d)>" % (self.id)

class ProjectHash(Base):
    '''
    classdocs
    '''
    __tablename__ = 'project_hash_test'
    id = Column(Integer, primary_key=True)
    project_hash = Column(String(32))
    project_fs_path = Column(String(2000))
    project_version = Column(String(200))

    def getVars(self):
        return vars(self)
    
    def __repr__(self):
        return "<hash (%d)>" % (self.project_hash)



class ProjectToMavenComponentMap(Base):
    '''
    classdocs
    '''
    __tablename__ = 'project_to_maven_component_map'
    projects_that_imports = Column(Integer,primary_key=True)
    maven_project_hash = Column(String(32), primary_key=True)
    maven_project_id = Column(Integer, primary_key=True)
    maven_project_name = Column(String(1024))
    maven_project_group = Column(String(1024))

    def getVars(self):
        return vars(self)
    
    def __repr__(self):
        return "<Post (%d)>" % (self.maven_project_hash)



"""
"""
class Projects(Base):
    '''
    classdocs
    '''
    __tablename__ = 'projects'
    project_id = Column(BigInteger, primary_key=True)
    project_type = Column(String(200))
    name = Column(String(1024))
    source = Column(String(1024))
    has_source = Column(Boolean)
    version = Column(String(1024))
    groop = Column(String(1024))
    description = Column(String(1024))
    path = Column(String(1024))
    hash=Column(String(32))

    def getVars(self):
        return vars(self)
    
    def __repr__(self):
        return "<Post (%d)>" % (self.project_id)


class source_projects_complete_info(Base):
    '''
    classdocs
    '''
    __tablename__ = 'source_projects_complete_info'
    project_id = Column(BigInteger, primary_key=True)
    project_type = Column(String(200))
    name = Column(String(1024))
    source = Column(String(1024))
    has_source = Column(Boolean)
    version = Column(String(1024))
    groop = Column(String(1024))
    description = Column(String(1024))
    path = Column(String(1024))
    num_files = Column(BigInteger)

    def getVars(self):
        return vars(self)
    
    def __repr__(self):
        return "<Post (%d)>" % (self.project_id)

class HashToFSpath(Base):
    __tablename__ = 'hash_to_fs_path'
    id = Column(Integer, primary_key=True)
    hash=Column(String(32))
    path= Column(String(4000))
    
    def getVars(self):
        return vars(self)
    
    def __repr__(self):
        return "<Post (%d)>" % (self.path)
