'''
Created on July 23, 2013

@author: vaibhavsaini
'''
import os
import sys
import time

from pymongo import Connection

from LoadFindBugInstances import LoadBugInstances


class MongoService():
    '''
    classdocs
    '''


    def __init__(self, config):
        '''
        Constructor
        '''
        self.config = config
        self.connection = Connection(self.config['host'], self.config['port'])
        self.db = self.connection[self.config['db']]
        self.sqlservice = LoadBugInstances(self.config['sql_config'])

    def loadData(self, start, end, window):
        i = start
        j = i + window
        while j <= end: 
            rows = self.sqlservice.readData(i, j)
            rows = self.getVars(rows)
            print "********", i, j
            i = j
            j += window
            collection = self.db.projects
            collection.insert(rows)
        print "returning from loadData"
    
    def loadPmcData(self, start, end, window):
        i = start
        j = i + window
        while j <= end: 
            rows = self.sqlservice.readPMCData(i, j)
            rows = self.getVars(rows)
            print "********", i, j
            i = j
            j += window
            collection = self.db.pmc_new
            collection.insert(rows)
        print "returning from loadData"

    def getVars(self, rows):
        ret = []
        for row in rows:
            obj = row.__dict__
            obj['_sa_instance_state'] = None
            ret.append(obj)
        return ret
    

    def writeToFile(self, text):
        file = open("dummy2.csv", 'a')
        try:
            file.write(text)
        finally:
            file.close()
    
    def writeToFilename(self, filename, text):
        file = open(filename, 'a')
        try:
            file.write(text)
        finally:
            file.close()

    def load_depth_of_inheritance(self, filename):
        file = open(filename, 'r')
        for line in file:
            columns = line.split(',')
            obj = {}
            obj['hash'] = columns[0]
            obj['sum'] = columns[1]
            obj['mean'] = columns[2]
            obj['median'] = columns[3]
            obj['min'] = columns[4]
            obj['max'] = columns[5]
            collection = self.db.depth_of_inheritance
            collection.insert(obj)
        file.close()

    def load_utilization(self, filename):
        file = open(filename, 'r')
        for line in file:
            columns = line.split(',')
            obj = {}
            obj['hash'] = columns[0].strip()
            obj['value'] = columns[1].strip()
            collection = self.db.fqn_uses
            collection.insert(obj)
        file.close()
    
    def loadFs_Uniquehash(self, filename):
        file = open(filename, 'r')
        for line in file:
            columns = line.split("=")
            obj = {}
            obj['hash'] = columns[1].strip()
            collection = self.db.unique_fs_hash
            collection.insert(obj)
        file.close()
        
    def loadFileData(self, filename):
        file = open("../input/" + filename, 'r')
        first_line = True
        collection_name = "j_" + filename.split(".")[0]
        collection = self.db[collection_name]
        collection.drop()
        for line in file:
            line = line.strip()
            if first_line:
                headers = line.split(",")
                first_line = False
            else:
                columns = line.split(",")
                obj = {}
                for i in range(0, len(columns)):
                    obj[headers[i]] = columns[i].strip()
                collection = self.db[collection_name]
                collection.insert(obj)
        file.close()
        
    def exportComponentStats(self, filename):
        cmd = """mongoexport --csv --fieldFile fields.txt --out p_component_usage_j_{0}.csv --db utilization --collection p_component_usage_j_{0}""".format(filename.split(".")[0])
        print("running command: ", cmd)
        os.system(cmd)

if __name__ == '__main__':
    dbConfig = { 'host': 'snake.ics.uci.edu',
                         'user': 'sourcerer',
                         'pass': 'tyl0n4pi',
                         'db': 'utilization'}
    osConfig = {"source": "/home/sourcerer/extracted/repo/asm/jars/maven",
                "dest": "/home/sourcerer/processedbugprofile"}
    sqlConfig = {"dbConfig":dbConfig,
              "osConfig": osConfig}
    config = {'sql_config': sqlConfig,
              'host' : 'localhost',
              'port' : 27017,
              'db' : 'utilization'
              }
    mservice = MongoService(config)
    print "starting load"
    
    for csvfile in os.listdir("../input"):
        if csvfile.endswith(".csv"):
            print "loading file: " + csvfile
            mservice.exportComponentStats(csvfile)
    #mservice.load_utilization("../input/utilization/fqn_uses.csv");
    # mservice.loadFs_Uniquehash("/home/sourcerer/unique_hash.txt")
    # mservice.loadData(0,196392,100000);
    # mservice.loadPmcData(0, 1957263, 100000);
    # args = sys.argv
    # mservice.writeToFilename("graph2.csv",mservice.tag_lang_csv(args[1]))
    print "loading done"
